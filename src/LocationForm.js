import React, { useState, useEffect } from 'react';
import axios from 'axios';

const LocationForm = () => {
  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [communes, setCommunes] = useState([]);
  const [resultSearch,setResultSearch]=useState([]);
  const [selectedProvince, setSelectedProvince] = useState('');
  const [selectedDistrict, setSelectedDistrict] = useState('');
  const [selectedCommune, setSelectedCommune] = useState('');
const [nameSearch,setNameSearch]=useState('');

  useEffect(() => {
    // Gọi API để lấy danh sách tỉnh/thành phố
    axios.get('https://vapi.vnappmob.com/api/province')
      .then(response => {
        const arrProvinnce = response.data.results;
        setProvinces(arrProvinnce);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);
  useEffect(() => {

    axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${nameSearch}&key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}`)
      .then(response => {
        setResultSearch(response.data);
        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, [nameSearch]);
  const handleProvinceChange = (e) => {
    const provinceId = e.target.value;
    setSelectedProvince(provinceId);
    setNameSearch(e.target.options[e.target.selectedIndex].text);
    // Gọi API để lấy danh sách huyện/quận của tỉnh/thành phố đã chọn
    axios.get(`https://vapi.vnappmob.com/api/province/district/${provinceId}`)
      .then(response => {
        setDistricts(response.data.results);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleDistrictChange = (e) => {
    const districtId = e.target.value;
    setSelectedDistrict(districtId);

    // Gọi API để lấy danh sách xã/phường của huyện/quận đã chọn
    axios.get(`https://vapi.vnappmob.com/api/province/ward/${districtId}`)
      .then(response => {
        setCommunes(response.data.results);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleCommuneChange = (e) => {
    const communeId = e.target.value;
    setSelectedCommune(communeId);
  };

  return (
    <div>
      <label>Tỉnh/Thành phố:</label>
      <select value={selectedProvince} onChange={handleProvinceChange}>
        <option value="">Chọn tỉnh/thành phố</option>
        {provinces.map(province => (
          <option key={province.province_id} value={province.province_id}>{province.province_name}</option>
        ))}
      </select>
      <label>Huyện/Quận:</label>
      <select value={selectedDistrict} onChange={handleDistrictChange}>
        <option value="">Chọn huyện/quận</option>
        {districts.map(district => (
          <option key={district.district_id} value={district.district_id}>{district.district_name}</option>
        ))}
      </select>
      <label>Xã/Phường:</label>
      <select value={selectedCommune} onChange={handleCommuneChange}>
        <option value="">Chọn xã/phường</option>
        {communes.map(commune => (
          <option key={commune.ward_id} value={commune.ward_id}>{commune.ward_name}</option>
        ))}
      </select>
    </div>
  );
};

export default LocationForm;
import React, { useEffect, useRef, useState } from 'react';
import {
  Box,
  Button,
  ButtonGroup,
  Flex,
  FormControl,
  FormLabel,
  HStack,
  IconButton,
  Input,
  Select,
  Text,
} from '@chakra-ui/react';
import {
  FaTimes,
  FaLocationArrow,
} from 'react-icons/fa';
import {
  GoogleMap,
  Marker,
  DirectionsRenderer,
  useJsApiLoader,
} from '@react-google-maps/api';
import axios from 'axios';

import LocationForm from './LocationForm';
const App = () => {
  const [map, setMap] = useState(null);
  const [directionsResponse, setDirectionsResponse] = useState(null);
  const [distance, setDistance] = useState('');
  const [duration, setDuration] = useState('');
  const originRef = useRef(null);
  const destinationRef = useRef(null);

  const [currentLocation, setCurrentLocation] = useState(null);

  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [communes, setCommunes] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState('');
  const [selectedDistrict, setSelectedDistrict] = useState('');
  const [selectedCommune, setSelectedCommune] = useState('');
  const [nameSearch, setNameSearch] = useState('');

  useEffect(() => {
    // Gọi API để lấy danh sách tỉnh/thành phố
    axios.get('https://vapi.vnappmob.com/api/province')
      .then(response => {
        const arrProvinnce = response.data.results;
        setProvinces(arrProvinnce);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    if (nameSearch) {
      axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${nameSearch}&key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}`)
        .then(response => {
          setCurrentLocation(response.data.results[0].geometry.location)
        })
        .catch(error => {
          console.log(error);
        });
    }

  }, [nameSearch]);


  const handleProvinceChange = (e) => {
    const provinceId = e.target.value;
    setSelectedProvince(provinceId);
    setNameSearch(e.target.options[e.target.selectedIndex].text);
    // Gọi API để lấy danh sách huyện/quận của tỉnh/thành phố đã chọn
    axios.get(`https://vapi.vnappmob.com/api/province/district/${provinceId}`)
      .then(response => {
        setDistricts(response.data.results);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleDistrictChange = (e) => {
    const districtId = e.target.value;
    setSelectedDistrict(districtId);
    setNameSearch(e.target.options[e.target.selectedIndex].text);
    // Gọi API để lấy danh sách xã/phường của huyện/quận đã chọn
    axios.get(`https://vapi.vnappmob.com/api/province/ward/${districtId}`)
      .then(response => {
        setCommunes(response.data.results);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleCommuneChange = (e) => {
    setNameSearch(e.target.options[e.target.selectedIndex].text);
    const communeId = e.target.value;
    setSelectedCommune(communeId);
  };


  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          setCurrentLocation({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          });
        },
        error => {
          console.error(error);
        }
      );
    } else {
      console.error('Trình duyệt không hỗ trợ Geolocation API');
    }
  }, []);

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
    libraries: ['places'],
  });

  const center = currentLocation || { lat: 21.028511, lng: 105.804817 };

  async function calculateRoute() {
    if (!originRef.current.value || !destinationRef.current.value) {
      return;
    }

    const directionsService = new window.google.maps.DirectionsService();
    const results = await directionsService.route({
      origin: originRef.current.value,
      destination: destinationRef.current.value,
      travelMode: window.google.maps.TravelMode.DRIVING,
    });

    setDirectionsResponse(results);
    setDistance(results.routes[0].legs[0].distance.text);
    setDuration(results.routes[0].legs[0].duration.text);
  }

  function clearRoute() {
    setDirectionsResponse(null);
    setDistance('');
    setDuration('');
    originRef.current.value = '';
    destinationRef.current.value = '';
  }

  if (!isLoaded) {
    return <div>Loading...</div>;
  }

  return (
    <Flex
      position='relative'
      flexDirection='column'
      alignItems='center'
      h='100vh'
      w='100vw'
    >
      <Box position='absolute' left={0} top={0} right={0} bottom={0} display='flex' justifyContent='center' alignItems='center'>
        {/* Google Map Box */}
        <Box
          width='80%'
          height='80%'
          maxWidth='800px'
          maxHeight='800px'
          borderRadius='lg'
          overflow='hidden'
        >
          <GoogleMap
            center={center}
            zoom={15}
            mapContainerStyle={{ width: '100%', height: '100%' }}
            options={{
              zoomControl: false,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl: false,
            }}
            onLoad={map => setMap(map)}
          >
            {currentLocation && <Marker position={currentLocation} />}
            {directionsResponse && (
              <DirectionsRenderer directions={directionsResponse} />
            )}
          </GoogleMap>
        </Box>
      </Box>

      <Box
        p={4}
        borderRadius='lg'
        m={4}
        bgColor='white'
        shadow='base'
        minW='container.md'
        zIndex='1'
      >
        <HStack spacing={2} justifyContent='space-between'>
          
          <div>
          <FormControl>
        <FormLabel>Tỉnh/Thành phố:</FormLabel>
        <Select value={selectedProvince} onChange={handleProvinceChange}>
          <option value="">Chọn tỉnh/thành phố</option>
          {provinces.map(province => (
            <option key={province.province_id} value={province.province_id}>{province.province_name}</option>
          ))}
        </Select>
      </FormControl>
      <FormControl>
      <FormLabel>Huyện/Quận:</FormLabel>
            <Select value={selectedDistrict} onChange={handleDistrictChange}>
              <option value="">Chọn huyện/quận</option>
              {districts.map(district => (
                <option key={district.district_id} value={district.district_id}>{district.district_name}</option>
              ))}
            </Select>
      </FormControl>
      <FormControl>
            <FormLabel>Xã/Phường:</FormLabel>
            <Select value={selectedCommune} onChange={handleCommuneChange}>
              <option value="">Chọn xã/phường</option>
              {communes.map(commune => (
                <option key={commune.ward_id} value={commune.ward_id}>{commune.ward_name}</option>
              ))}
            </Select>
            </FormControl>
          </div>
          <Box flexGrow={1}>
            <Input type='text' placeholder='Origin' ref={originRef} />
          </Box>
          <Box flexGrow={1}>
            <Input
              type='text'
              placeholder='Destination'
              ref={destinationRef}
            />

          </Box>

          <ButtonGroup>
            <Button colorScheme='pink' type='submit' onClick={calculateRoute}>
              Calculate Route
            </Button>
            <IconButton
              aria-label='Clear route'
              icon={<FaTimes />}
              onClick={clearRoute}
            />
          </ButtonGroup>
        </HStack>
        <HStack spacing={4} mt={4} justifyContent='space-between'>
          <Text>Distance: {distance}</Text>
          <Text>Duration: {duration}</Text>
          <IconButton
            aria-label='Center map'
            icon={<FaLocationArrow />}
            isRound
            onClick={() => {
              if (map) {
                map.panTo(center);
                map.setZoom(15);
              }
            }}
          />
        </HStack>
      </Box>
    </Flex>
  );
};

export default App;